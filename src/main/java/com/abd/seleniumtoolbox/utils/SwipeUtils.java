package com.abd.seleniumtoolbox.utils;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import java.time.Duration;

public class SwipeUtils {

    private static final int SWIPE_SPEED_MILLIS = 500;
    private AndroidDriver androidDriver;

    @Contract(pure = true)
    public SwipeUtils(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    @Contract(pure = true)
    public static double getPercentage(double num) {
        return num / 100;
    }

    public void swipeRightToLeft(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int startX = x1 + (int) (size.width * getPercentage(60));
        int endX = x1 + (int) (size.width * getPercentage(30));

        int startY = (y1 + y2) / 2;
        int endY = startY;

        swipe(startX, startY, endX, endY);
    }

    public void swipeRightToLeftCompletely(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int startX = x1 + (int) (size.width * getPercentage(99));
        int endX = x1 + (int) (size.width * getPercentage(1));

        int startY = (y1 + y2) / 2;
        int endY = startY;

        swipe(startX, startY, endX, endY);
    }

    public void swipeLeftToRight(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int startX = x1 + (int) (size.width * getPercentage(30));
        int endX = x1 + (int) (size.width * getPercentage(60));

        int startY = (y1 + y2) / 2;
        int endY = startY;

        swipe(startX, startY, endX, endY);
    }

    public void swipeLeftToRightCompletely(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int startX = x1 + (int) (size.width * getPercentage(1));
        int endX = x1 + (int) (size.width * getPercentage(99));

        int startY = (y1 + y2) / 2;
        int endY = startY;

        swipe(startX, startY, endX, endY);
    }

    public void swipeElementBottomToTop(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int startX = (x1 + x2) / 2;
        int endX = startX;

        int startY = y1 + (int) (size.height * getPercentage(60));
        int endY = y1 + (int) (size.height * getPercentage(30));

        swipe(startX, startY, endX, endY);
    }

    public void swipeElementTopToBottom(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int startX = (x1 + x2) / 2;
        int endX = startX;

        int startY = y1 + (int) (size.height * getPercentage(30));
        int endY = y1 + (int) (size.height * getPercentage(60));

        swipe(startX, startY, endX, endY);
    }

    public void swipeScreenBottomToTop() {
        int x1 = 0;
        int y1 = 0;

        Dimension dimension = androidDriver.manage().window().getSize();
        int x2 = dimension.width;
        int y2 = dimension.height;

        int startX = (x1 + x2) / 2;
        int endX = startX;

        int startY = y1 + (int) (dimension.height * getPercentage(60));
        int endY = y1 + (int) (dimension.height * getPercentage(30));

        swipe(startX, startY, endX, endY);
    }

    public void swipeScreenTopToBottom() {
        int x1 = 0;
        int y1 = 0;

        Dimension dimension = androidDriver.manage().window().getSize();
        int x2 = dimension.width;
        int y2 = dimension.height;

        int startX = (x1 + x2) / 2;
        int endX = startX;

        int startY = y1 + (int) (dimension.height * getPercentage(30));
        int endY = y1 + (int) (dimension.height * getPercentage(60));

        swipe(startX, startY, endX, endY);
    }

    public void swipe(
            int startX,
            int startY,
            int endX,
            int endY) {
        TouchAction action = new TouchAction(androidDriver);
        try {
            action.press(new PointOption().withCoordinates(startX, startY))
                    .waitAction(new WaitOptions().withDuration(Duration.ofMillis(SWIPE_SPEED_MILLIS)))
                    .moveTo(new PointOption().withCoordinates(endX, endY))
                    .release()
                    .perform();
        } catch (WebDriverException e) {
        }
    }
}
