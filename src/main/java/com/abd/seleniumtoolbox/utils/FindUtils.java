package com.abd.seleniumtoolbox.utils;

import io.appium.java_client.android.AndroidDriver;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import static org.junit.Assert.assertNotNull;

public class FindUtils {

    private static final int MAX_TRIES = 5;
    private static final String DIRECTION_DOWNWARDS = "down";
    private static final String DIRECTION_UPWARDS = "up";

    private AndroidDriver driver;
    private long implicitWait;
    private boolean keyboardDisabled;

    @Contract(pure = true)
    public FindUtils(
            AndroidDriver driver,
            long implicitWait,
            boolean keyboardDisabled) {
        this.driver = driver;
        this.implicitWait = implicitWait;
        this.keyboardDisabled = keyboardDisabled;
    }

    public WebElement scrollScreenToElement(
            String name,
            @NotNull By by,
            boolean reverseDirection) {
        return scrollToElement(name, by, null, reverseDirection);
    }

    public WebElement scrollScreenToElement(
            String name,
            @NotNull By by) {
        return scrollToElement(name, by, null, false);
    }

    public WebElement scrollElementToElement(
            String name,
            @NotNull By scrollBy,
            @NotNull By by,
            boolean reverseDirection) {
        return scrollToElement(name, by, scrollBy, reverseDirection);
    }

    public WebElement scrollElementToElement(
            String name,
            @NotNull By scrollBy,
            @NotNull By by) {
        return scrollToElement(name, by, scrollBy, false);
    }

    private WebElement scrollToElement(
            String name,
            @NotNull By by,
            By scrollBy,
            boolean reverseDirection) {
        setImplicitWait(2);

        if (name != null && !name.equals("")) System.out.println("[TEST] Finding: " + name);

        String DIRECTION_FIRST, DIRECTION_SECOND;

        if (!reverseDirection) {
            DIRECTION_FIRST = DIRECTION_DOWNWARDS;
            DIRECTION_SECOND = DIRECTION_UPWARDS;
        } else {
            DIRECTION_FIRST = DIRECTION_UPWARDS;
            DIRECTION_SECOND = DIRECTION_DOWNWARDS;
        }

        WebElement webElement = scrollToDirection(name, by, scrollBy, DIRECTION_FIRST);
        if (webElement == null) webElement = scrollToDirection(name, by, scrollBy, DIRECTION_SECOND);
        if (webElement == null) {
            if (name != null && !name.equals(""))
                System.out.println("[TEST] [FAILED] Unable to find: " + name);
            else
                System.out.println("[TEST] [FAILED] Unable to find element");
        }

        setImplicitWait(implicitWait);
        assertNotNull(webElement);

        return webElement;
    }

    @SuppressWarnings({
            "checkstyle:CyclomaticComplexity"})
    public WebElement scrollToDirection(
            String name,
            @NotNull By by,
            By scrollBy,
            @NotNull String direction) {

        WebElement webElement = null;
        boolean found = false;
        int tries = MAX_TRIES;

        do {
            try {
                webElement = findElement(by);
                if (webElement != null && webElement.isDisplayed()) {
                    if (name != null && !name.equals("")) System.out.println("[TEST] Found: " + name);
                    found = true;
                }
            } catch (NoSuchElementException e) {
                if (name != null && !name.equals(""))
                    System.out.println("[TEST] Unable to find: " + name + ", scrolling " + direction + "... " + tries);
                else
                    System.out.println("[TEST] Unable to find element, scrolling " + direction + "... " + tries);

                WebElement scrollElement = null;

                if (scrollBy != null) {
                    scrollElement = findElement(scrollBy);
                    if (scrollElement == null) {
                        System.out.println("[TEST] Unable to find the scrollable element, scrolling the screen instead...");
                    }
                }

                SwipeUtils swipeUtils = new SwipeUtils(driver);
                if (direction.equals("down")) {
                    if (scrollElement == null) swipeUtils.swipeScreenBottomToTop();
                    else swipeUtils.swipeElementBottomToTop(scrollElement);
                } else if (direction.equals("up")) {
                    if (scrollElement == null) swipeUtils.swipeScreenTopToBottom();
                    else swipeUtils.swipeElementTopToBottom(scrollElement);
                }

                tries--;
            }
        } while (!found && tries > 0);

        return webElement;
    }

    public void setImplicitWait(long seconds) {
        driver
                .manage()
                .timeouts()
                .implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    public WebElement findElement(@NotNull By by) {
        return findElement(null, by);
    }

    public WebElement findElement(
            String name,
            @NotNull By by) {

        new KeyboardUtils(driver, driver, keyboardDisabled).hideKeyboard();

        if (name != null && !name.equals("")) System.out.println("[TEST] Finding: " + name);

        FluentWait<AndroidDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofMinutes(1))
                .pollingEvery(Duration.ofSeconds(1));

        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }
}
