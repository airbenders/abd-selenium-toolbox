package com.abd.seleniumtoolbox.utils;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.AndroidServerFlag;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.remote.service.DriverService;

public class AppiumServiceManager {

    public AppiumDriverLocalService startServer(File logFile, Integer port, Integer bootstrapPort) {
        System.out.println("[SERVER] Appium Service Starting...");

        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withIPAddress("127.0.0.1");

        if (port != null) {
            builder.usingPort(port);
            System.out.println("[SERVER] Port: " + port);
        } else {
            builder.usingAnyFreePort();
        }

        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);

        if (logFile != null) {
            builder.withLogFile(logFile);
            System.out.println("[SERVER] Log File: " + logFile.getAbsolutePath());
        }

        if (bootstrapPort != null) {
            builder.withArgument(AndroidServerFlag.BOOTSTRAP_PORT_NUMBER, String.valueOf(bootstrapPort));
            System.out.println("[SERVER] Bootstrap Port: " + bootstrapPort);
        }

        AppiumDriverLocalService service = AppiumDriverLocalService.buildService(builder);
        service.clearOutPutStreams();
        service.start();

        return service;
    }

    public void stopServer(@NotNull DriverService service) {
        service.stop();
        System.out.println("[SERVER] Appium Service Stopped");
    }

    public boolean checkIfServerIsRunnning(int port) {
        boolean isServerRunning = false;
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            serverSocket.close();
        } catch (IOException e) {
            isServerRunning = true;
        } finally {
            serverSocket = null;
        }
        return isServerRunning;
    }
}
