package com.abd.seleniumtoolbox.utils;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

public class TapUtils {

    private PerformsTouchActions performsTouchActions;

    @Contract(pure = true)
    public TapUtils(PerformsTouchActions performsTouchActions) {
        this.performsTouchActions = performsTouchActions;
    }

    public void singleTap(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int targetX = (x1 + x2) / 2;
        int targetY = (y1 + y2) / 2;

        tap(targetX, targetY);
    }

    public void doubleTap(@NotNull WebElement element) {
        int x1 = element.getLocation().getX();
        int y1 = element.getLocation().getY();

        Dimension size = element.getSize();
        int x2 = x1 + size.width;
        int y2 = y1 + size.height;

        int targetX = (x1 + x2) / 2;
        int targetY = (y1 + y2) / 2;

        tap(targetX, targetY);
        tap(targetX, targetY);
    }

    private void tap(int targetX,
                     int targetY) {
        TouchAction action = new TouchAction(performsTouchActions);
        System.out.println("[TEST] Tapping: X=" + targetX + ";Y=" + targetY);
        action.tap(PointOption.point(targetX, targetY))
                .tap(PointOption.point(targetX, targetY))
                .perform();
    }
}
