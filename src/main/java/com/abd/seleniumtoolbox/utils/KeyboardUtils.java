package com.abd.seleniumtoolbox.utils;

import io.appium.java_client.HasOnScreenKeyboard;
import io.appium.java_client.HidesKeyboard;
import org.jetbrains.annotations.Contract;
import org.openqa.selenium.WebDriverException;

public class KeyboardUtils {

    private HasOnScreenKeyboard hasOnScreenKeyboard;
    private HidesKeyboard hidesKeyboard;
    private boolean keyboardDisabled;

    @Contract(pure = true)
    public KeyboardUtils(
            HasOnScreenKeyboard hasOnScreenKeyboard,
            HidesKeyboard hidesKeyboard,
            boolean keyboardDisabled) {
        this.hasOnScreenKeyboard = hasOnScreenKeyboard;
        this.hidesKeyboard = hidesKeyboard;
        this.keyboardDisabled = keyboardDisabled;
    }

    public void hideKeyboard() {
        if (!keyboardDisabled) {
            if (hasOnScreenKeyboard.isKeyboardShown()) {
                try {
                    hidesKeyboard.hideKeyboard();
                } catch (WebDriverException ignored) {
                }
            }
        }
    }
}
